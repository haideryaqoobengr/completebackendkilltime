var express = require('express');
var router = express.Router();
var pool = require('../db')
var multer = require('multer')
var jwt = require('jsonwebtoken');
var fs = require('fs');

var {
    Validator
} = require('node-input-validator');
require('dotenv').config();
// var session = require('express-session');
var d = new Date();
var time = d.getTime();
// return res.sendStatus(440);

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/images')
    },
    filename: function (req, file, cb) {
        cb(null, time + '_' + file.originalname)
    }
})

var upload = multer({
    storage: storage
});



// Login
router.post('/login', function (req, res, next) {
    const v = new Validator(req.body, {
        email: 'required|email',
        password: 'required'
    });

    v.check().then((matched) => {
        if (!matched) {
            res.status(422).send(v.errors);
        }
    });
    var email = req.body.email;
    var password = req.body.password;

    pool.query("select * from users where email= '" + req.body.email + "' && password='" + req.body.password + "'  ")
        .then(async (rows) => {
            if (rows.length == 1) {
                const access_token = jwt.sign({
                    email: email
                }, process.env.ACCESS_TOKEN_SECRET, {
                    expiresIn: process.env.JWT_EXPIRES_IN
                })


                var profile_status = "online"
                console.log("id during login " + rows[0].id);

                const rows2 = await pool.query("UPDATE users SET status = '" + profile_status + "' WHERE email = '" + email + "' AND password = '" + password + "'  ");

                pool.query("select profile_photo_path from users where id= '" + rows[0].id + "' ")
                    .then((rows3) => {


                        if (rows3[0].profile_photo_path == null || rows3[0].profile_photo_path == "") {
                            console.log("image not exist");

                        } else {
                            const path = 'public/images/' + rows3[0].profile_photo_path;
                            const bitmap = fs.readFileSync(path);
                            var image = bitmap.toString('base64');
                            rows3[0].profile_photo = "data:image/png;base64," + image;
                        }



                        res.contentType('text/plain');

                        res.send({
                            status: "OK",
                            id: rows[0].id,
                            token: access_token,
                            profile_photo: rows3
                        });

                        // res.send(rows);

                    })
                    .catch(err => {
                        //handle error
                        console.log(err);

                    })



            } else {
                res.sendStatus(404);

            }
        })
        .catch(err => {
            //handle error
            console.log(err);
            // conn.end();
        })

});

//GET PIC for profile
router.get('/user_pic', verifyToken, function (req, res) {

    pool.query("select profile_photo_path,name,country from users where id= '" + req.query.user_id + "' ")
        .then((rows3) => {
            if (rows3[0].profile_photo_path == null || rows3[0].profile_photo_path == "") {
                console.log("image not exist");
            } else {
                const path = 'public/images/' + rows3[0].profile_photo_path;
                const bitmap = fs.readFileSync(path);
                var image = bitmap.toString('base64');
                rows3[0].profile_photo = "data:image/png;base64," + image;
            }
            res.contentType('text/plain');

            res.send({
                status: "OK",
                name:rows3[0].name,
                profile_photo: rows3[0].profile_photo,
                state:rows3[0].country
            });
        })
        .catch(err => {
            //handle error
            console.log(err);
        })
})
// upload.single('profile_photo')
// Create profile
router.post('/create_profile', verifyToken, upload.single('profile_photo'), function (req, res, next) {

    // var profile_photo_path = req.body.profile_photo;

    var profile_photo_path = req.file.filename;
    console.log(profile_photo_path);
    var relationship_status = req.body.relationship_status;
    var education = req.body.education;
    // var country = req.body.country;
    var bio = req.body.bio;
    console.log("bio", bio);
    var languages = req.body.languages;
    // var user_email = req.session.email;
    var lang = languages.toString();
    var id = req.query.id;

    pool.query("UPDATE users SET profile_photo_path = '" + profile_photo_path + "',relationship_status = '" + relationship_status + "',education = '" + education + "',bio = '" + bio + "',languages = '" + languages + "' WHERE id = '" + id + "' ")
        .then((rows) => {
            res.json({
                status: "OK"
            });

        })
        .catch(err => {
            //handle error
            console.log("Error =" + err);
            // conn.end();
        })


});
// edit profile
router.post('/edit_profile', verifyToken, upload.single('profile_photo'), function (req, res, next) {
    var name = req.body.name;

    var profile_photo_path = req.file.filename;
    console.log(profile_photo_path);
    var password = req.body.password;
    // var gender = req.bodsy.gender;
    var location = req.body.location;
    var day = req.body.day;
    var month = req.body.month;
    var year = req.body.year;
    var bio = req.body.bio;
    var languages = req.body.languages;
    // var profile_photo_path = req.body.profile_photo;
    var relationship_status = req.body.relationship_status;
    var education = req.body.education;
    var country = req.body.country;
    // var user_email = req.session.email;
    var id = req.query.id;

    pool.query("UPDATE users SET year = '" + year + "',month = '" + month + "',day = '" + day + "',location = '" + location + "',name = '" + name + "',profile_photo_path = '" + profile_photo_path + "',relationship_status = '" + relationship_status + "',education = '" + education + "',country = '" + country + "',bio = '" + bio + "',languages = '" + languages + "' WHERE id = '" + id + "' ")
        .then((rows) => {
            res.json({
                status: "OK"
            });

        })
        .catch(err => {
            //handle error
            console.log("Error =" + err);
            // conn.end();
        })
});
// Upload a photo
router.post('/upload_photo', verifyToken, upload.single('image'), function (req, res, next) {

    // var image_name = req.body.image;
    var image_name = req.file.filename;
    console.log(image_name);
    console.log("image name in upload photo " + image_name);
    var id = 0;
    // var user_email = req.session.email;
    var user_id = req.query.id;
    console.log("User_id in upload photo " + user_id);

    pool.query("INSERT INTO photos value (?,?,?)", [image_name, id, user_id])
        .then((rows) => {
            res.json({
                status: "OK"
            });

        })
        .catch(err => {
            //handle error
            console.log(err);
            res.json({
                status: "NOT Inserted"
            });
            // conn.end();
        })

});
// Get Personal Profile
var data = [];
router.get('/profile', verifyToken, function (req, res, next) {

    var user_id = req.query.id;


    pool.query("select name,id,location,day,month,year,profile_photo_path,relationship_status,education,country,bio,languages from users where id= '" + user_id + "' ")
        .then((rows) => {
            const dt = new Date();
            var current_year = dt.getFullYear();

            rows[0].age = current_year - rows[0].year;

            if (rows[0].profile_photo_path == null || rows[0].profile_photo_path == "") {
                console.log("image not exist");
            } else {
                console.log("image: " + rows[0].profile_photo_path);

                const path = 'public/images/' + rows[0].profile_photo_path;
                const bitmap = fs.readFileSync(path);
                var image = bitmap.toString('base64');
                rows[0].profile_photo = "data:image/png;base64," + image;
            }
            var str = rows[0].languages;
            var arr = str.split(",")
            rows[0].languages = arr;
            data = rows[0];
            pool.query("select image_name,id from photos where user_id= '" + user_id + "' ")
                .then((rows) => {

                    var len = rows.length;
                    var i;
                    console.log("Total Media= " + len);
                    for (i = 0; i < len; i++) {
                        if (rows[i].image_name == null || rows[i].image_name == "") {
                            console.log("image not exist");

                        } else {
                            const path = 'public/images/' + rows[i].image_name;
                            const bitmap = fs.readFileSync(path);
                            var image = bitmap.toString('base64');
                            rows[i].media = "data:image/png;base64," + image;
                        }
                    }
                    res.contentType('text/plain');
                    res.send({
                        status: "OK",
                        data: data,
                        photos: rows
                    });

                })
                .catch(err => {
                    //handle error
                    console.log(err);

                })
        })
        .catch(err => {
            //handle error
            console.log(err);
        })
});

// Edit profile request

var data = [];
router.get('/edit_personal_profile', verifyToken, function (req, res, next) {

    var user_id = req.query.id;
    pool.query("select name,id,location,day,month,year,profile_photo_path,relationship_status,education,country,bio,languages,status from users where id= '" + user_id + "' ")
        .then((rows) => {
            const dt = new Date();
            var current_year = dt.getFullYear();

            rows[0].age = current_year - rows[0].year;

            if (rows[0].profile_photo_path == null || rows[0].profile_photo_path == "") {
                console.log("image not exist");

            } else {
                const path = 'public/images/' + rows[0].profile_photo_path;
                const bitmap = fs.readFileSync(path);
                var image = bitmap.toString('base64');
                rows[0].profile_photo = "data:image/png;base64," + image;
            }


            var str = rows[0].languages;
            var arr = str.split(",")
            rows[0].languages = arr;
            res.contentType('text/plain');
            res.send(rows);
        })
        .catch(err => {
            //handle error
            console.log(err);
        })
});

// Get Specific Profile
router.get('/discover/profile', verifyToken, function (req, res, next) {

    var user_id = req.query.user_id;
    pool.query("select name,id,location,day,month,year,profile_photo_path,relationship_status,education,country,bio,languages,status from users where id= '" + user_id + "' ")
        .then((rows) => {
            console.log(rows[0]);
            if (rows[0].profile_photo_path == null || rows[0].profile_photo_path == "") {
                console.log("image not exist");

            } else {
                const path = 'public/images/' + rows[0].profile_photo_path;
                const bitmap = fs.readFileSync(path);
                var image = bitmap.toString('base64');
                rows[0].profile_photo = "data:image/png;base64," + image;
            }


            var str = rows[0].languages;
            var arr = str.split(",")
            rows[0].languages = arr;


            const dt = new Date();
            var current_year = dt.getFullYear();

            rows[0].age = current_year - rows[0].year;

            res.contentType('text/plain');
            res.send(rows);
        })
        .catch(err => {
            //handle error
            console.log(err);
        })
});
var year;
// Get All Profiles Related to age
router.get('/discover', verifyToken, function (req, res) {
    var query_id = req.query.id;
    console.log("In discover Query id= "+query_id);

    pool.query("SELECT * FROM filters WHERE user_id='" + query_id + "'").then(async rows => {
        console.log("No of Rows of user in filters = " + rows.length);
        if (rows.length == 1) {
            var query_id = req.query.id;
            var upper_age = rows[0].upper_age;
            var lower_age = rows[0].lower_age;
            var state = rows[0].state;
            var gender = rows[0].gender;
            console.log("Upper_age: " + upper_age);
            console.log("lower_age: " + lower_age);
            console.log("Gender: " + gender);
            console.log("State: " + state);
            console.log("In Discover for search");

            if ((upper_age != "") && (lower_age != "") && (gender != "") && (state != "")) {
                console.log("1");

                var arr = [];
                var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where  id NOT IN ('" + query_id + "') AND gender = '" + gender + "' AND country = '" + state + "'  ")
                var i;
                const dt = new Date();
                var current_year = dt.getFullYear();
                var len = rows.length;
                var age;
                console.log(len);
                for (i = 0; i < len; i++) {
                    rows[i].age = current_year - rows[i].year;
                    console.log("i=" + i);
                    console.log("age=" + rows[i].age);
                    if (rows[i].age >= lower_age && rows[i].age <= upper_age) {
                        arr.push(rows[i])
                    }

                    if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                        console.log("image not exist");

                    } else {
                        const path = 'public/images/' + rows[i].profile_photo_path;
                        const bitmap = fs.readFileSync(path);
                        var image = bitmap.toString('base64');
                        rows[i].profile_photo = "data:image/png;base64," + image;
                    }
                }
                res.contentType('text/plain');
                res.send(arr);
            } else if ((upper_age != "") && (lower_age != "") && (gender != "")) {
                console.log("2");
                var arr = [];
                var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where  id NOT IN ('" + query_id + "') AND gender = '" + gender + "'  ")
                var i;
                const dt = new Date();
                var current_year = dt.getFullYear();
                var len = rows.length;
                var age;
                console.log(len);
                for (i = 0; i < len; i++) {
                    rows[i].age = current_year - rows[i].year;
                    console.log("i=" + i);
                    console.log("age=" + rows[i].age);
                    if (rows[i].age >= lower_age && rows[i].age <= upper_age) {
                        arr.push(rows[i])
                    }

                    if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                        console.log("image not exist");

                    } else {
                        const path = 'public/images/' + rows[i].profile_photo_path;
                        const bitmap = fs.readFileSync(path);
                        var image = bitmap.toString('base64');
                        rows[i].profile_photo = "data:image/png;base64," + image;
                    }
                }
                res.contentType('text/plain');
                res.send(arr);

            } else if ((upper_age != "") && (lower_age != "") && (state != "")) {
                console.log("3");
                var arr = [];
                var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where  id NOT IN ('" + query_id + "')  AND country = '" + state + "'  ")
                var i;
                const dt = new Date();
                var current_year = dt.getFullYear();
                var len = rows.length;
                var age;
                console.log(len);
                for (i = 0; i < len; i++) {
                    rows[i].age = current_year - rows[i].year;
                    console.log("i=" + i);
                    console.log("age=" + rows[i].age);
                    if (rows[i].age >= lower_age && rows[i].age <= upper_age) {
                        arr.push(rows[i])
                    }

                    if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                        console.log("image not exist");

                    } else {
                        const path = 'public/images/' + rows[i].profile_photo_path;
                        const bitmap = fs.readFileSync(path);
                        var image = bitmap.toString('base64');
                        rows[i].profile_photo = "data:image/png;base64," + image;
                    }
                }
                res.contentType('text/plain');
                res.send(arr);
            } else if ((gender != "") && (state != "")) {
                console.log("4");
                var arr = [];
                var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where   gender = '" + gender + "' AND country = '" + state + "'  ")
                var i;
                const dt = new Date();
                var current_year = dt.getFullYear();
                var len = rows.length;
                var age;
                console.log(len);
                for (i = 0; i < len; i++) {
                    rows[i].age = current_year - rows[i].year;
                    console.log("i=" + i);
                    console.log("age=" + rows[i].age);
                    // if(rows[i].age >= lower_age && rows[i].age <=upper_age){
                    //     arr.push(rows[i])
                    // }

                    if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                        console.log("image not exist");

                    } else {
                        const path = 'public/images/' + rows[i].profile_photo_path;
                        const bitmap = fs.readFileSync(path);
                        var image = bitmap.toString('base64');
                        rows[i].profile_photo = "data:image/png;base64," + image;
                    }
                }
                res.contentType('text/plain');
                res.send(rows);
            } else if ((upper_age != "") && (lower_age != "")) {
                console.log("5");
                var arr = [];
                var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where  id NOT IN ('" + query_id + "')   ")
                var i;
                const dt = new Date();
                var current_year = dt.getFullYear();
                var len = rows.length;
                var age;
                console.log(len);
                for (i = 0; i < len; i++) {
                    rows[i].age = current_year - rows[i].year;
                    console.log("i=" + i);
                    console.log("age=" + rows[i].age);
                    if (rows[i].age >= lower_age && rows[i].age <= upper_age) {
                        arr.push(rows[i])
                    }

                    if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                        console.log("image not exist");

                    } else {
                        const path = 'public/images/' + rows[i].profile_photo_path;
                        const bitmap = fs.readFileSync(path);
                        var image = bitmap.toString('base64');
                        rows[i].profile_photo = "data:image/png;base64," + image;
                    }
                }
                res.contentType('text/plain');
                res.send(arr);
            } else if (gender != "") {
                console.log("6");
                var arr = [];
                var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where  id NOT IN ('" + query_id + "') AND gender = '" + gender + "'  ")
                var i;
                const dt = new Date();
                var current_year = dt.getFullYear();
                var len = rows.length;
                var age;
                console.log(len);
                for (i = 0; i < len; i++) {
                    rows[i].age = current_year - rows[i].year;
                    console.log("i=" + i);
                    console.log("age=" + rows[i].age);
                    // if(rows[i].age >= lower_age && rows[i].age <=upper_age){
                    //     arr.push(rows[i])
                    // }

                    if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                        console.log("image not exist");

                    } else {
                        const path = 'public/images/' + rows[i].profile_photo_path;
                        const bitmap = fs.readFileSync(path);
                        var image = bitmap.toString('base64');
                        rows[i].profile_photo = "data:image/png;base64," + image;
                    }
                }
                // res.contentType('text/plain');
                res.send(rows);
            } else if (state != "") {
                console.log("7");
                var arr = [];
                var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where  id NOT IN ('" + query_id + "') AND  country = '" + state + "'  ")
                var i;
                const dt = new Date();
                var current_year = dt.getFullYear();
                var len = rows.length;
                var age;
                console.log(len);
                for (i = 0; i < len; i++) {
                    rows[i].age = current_year - rows[i].year;
                    console.log("i=" + i);
                    console.log("age=" + rows[i].age);
                    // if(rows[i].age >= lower_age && rows[i].age <=upper_age){
                    //     arr.push(rows[i])
                    // }

                    if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                        console.log("image not exist");

                    } else {
                        const path = 'public/images/' + rows[i].profile_photo_path;
                        const bitmap = fs.readFileSync(path);
                        var image = bitmap.toString('base64');
                        rows[i].profile_photo = "data:image/png;base64," + image;
                    }
                }
                res.contentType('text/plain');
                res.send(rows);
            } else {
                res.json("Please Fill at least one the Fields")
            }


        } else if (rows.length == 0) {
            console.log("in else if statement Query id =" + req.query.id);

            pool.query("select * from users where id= '" + req.query.id + "' ")
                .then(async rows => {
                    console.log("year " + rows[0].year);
                  var   year = rows[0].year;
                    var x = parseInt(year) + 10;
                    var y = -10;
                    var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where id NOT IN ('" + req.query.id + "') ")
                    var i;
                    const dt = new Date();
                    var current_year = dt.getFullYear();
                    var len = rows.length;
                    console.log(len);
                    for (i = 0; i < len; i++) {
                        rows[i].age = current_year - rows[i].year;
                        console.log("i=" + i);
                        console.log("age=" + rows[i].age);
                        if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                            console.log("image not exist");

                        } else {
                            const path = 'public/images/' + rows[i].profile_photo_path;
                            const bitmap = fs.readFileSync(path);
                            var image = bitmap.toString('base64');
                            rows[i].profile_photo = "data:image/png;base64," + image;
                        }
                    }
                    res.contentType('text/plain');
                    res.send(rows);
                })
                .catch(err => {
                    //handle error
                    console.log(err);
                    // conn.end();
                })

        }




    }).catch(err => {

    })

});

// Filter(search) with specific credentials
router.post('/search',  function (req, res, next) {
    var user_id = req.query.id;
    var upper_age = req.body.upper_age;
    var lower_age = req.body.lower_age;
    var state = req.body.state;
    var gender = req.body.gender;
    var id =0;
    console.log("Upper_age: " + upper_age);
    console.log("lower_age: " + lower_age);
    console.log("Gender: " + gender);
    console.log("State: " + state);
    console.log("In search");
    pool.query("SELECT * FROM filters WHERE user_id='"+user_id+"'").then(rows=>{
if(rows.length==1){
    console.log("In search route to update filter user_id="+req.query.id);

    pool.query("UPDATE filters SET  upper_age= '" + upper_age + "',lower_age = '" + lower_age + "',gender = '" + gender + "',state = '" + state + "' WHERE user_id = '" + req.query.id + "' ")
    .then((rows) => {
        res.json({
            status: "OK"
        });

    })
    .catch(err => {
        //handle error
        console.log("Error =" + err);
        // conn.end();
    })
}
else{
    console.log("In search route to insert filter user_id="+req.query.id);
    
    pool.query("INSERT INTO filters value (?,?,?,?,?,?)", [id, user_id, upper_age,lower_age,gender,state])
    .then((rows) => {
        res.json({
            status: "OK"
        });
    })
    .catch(err => {
        //handle error
        console.log(err);
        res.json({
            status: "NOT Inserted"
        });
        // conn.end();
    })

}
    }).catch(err=>{
        console.log("Error =" + err);

    })
});




// logout
router.get('/logout', verifyToken, function (req, res, next) {
    // req.session.destroy();
    //   res.render('index', { title: 'Express'});
    var id = req.query.id;
    var profile_status = "offline"
    console.log("id during logout " + id);

    pool.query("UPDATE users SET status = '" + profile_status + "' WHERE id = '" + id + "' ")
        .then((rows) => {
            res.json({
                status: "OK",
            });
        })
        .catch(err => {
            //handle error
            console.log("Error =" + err);
            // conn.end();
        })

});



function verifyToken(req, res, next) {
    console.log("varify  " + req);

    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, (err, authData) => {
            if (err) {
                console.log("forbidden inner");
                return res.sendStatus(403);
            }
            // req.user = user;
            else {
                next();
            }
        });

    } else {
        // Forbidden
        console.log("forbidden outer");
        res.sendStatus(403);
    }
}

router.get('/*', function (req, res) {
    res.status(404).send('Page Not Exist');
});


router.post('/*', function (req, res) {
    res.status(404).send('Page Not Exist');
});


module.exports = router;