var express = require('express');
var router = express.Router();
var pool = require('../db')
var jwt = require('jsonwebtoken');
// var {Validator} = require('node-input-validator');
var app = express();

var http = require('http');
var socketio = require('socket.io');
var server =http.createServer(app)
var io = socketio(server);



require('dotenv').config();
/* GET home page. */
const rooms = {}

app.get('/', (req, res) => {
  res.render('loginn', {
    rooms: rooms
  })
})

app.post('/loginn', (req, res) => {
  var email = req.body.email
  pool.query("select * from users where user_email  NOT IN ('" + email + "')").then(rows => {
    res.render('allusers', {
      users: rows,
      user_email:email
    })
  }).catch(err => {
    console.log("Error " + err);
  });
})

app.get('/create_room', (req, res) => {
  var user_1 = req.query.user_1;
  var user_2 = req.query.user_2;
  var room_id = 0;
  pool.query("select * from rooms where user_1='"+user_1+"' AND user_2='"+user_2+"' OR user_1='"+user_2+"' AND user_2='"+user_1+"'").then(rows=>{
    if(rows.length >= 1){
      rooms[rows[0].room_id] = {
        users: {}
      }
      var q=rows[0].room_id;
  
      console.log("Q="+q);
      
      res.redirect('/msg?room_id='+rows[0].room_id+'&user_id='+user_1)
      // io.emit('room-created', rows[0].id)
    }
    else{
      pool.query("INSERT INTO rooms value(?,?,?)", [room_id, user_1, user_2]).then(rows => {
        console.log("new room created in data base");
        pool.query("select room_id from rooms where user_1= '" + user_1 + "' AND user_2= '" + user_2 + "'").then(rows => {
          console.log("id "+rows[0].room_id);
          rooms[rows[0].room_id] = {
            users: {}
          }
          res.redirect('/msg?room_id='+rows[0].room_id+'&user_id='+user_1)
          // io.emit('room-created', rows[0].id)
    
        }).catch(err => {
          console.log("err " + err);
    
        })
    
      }).catch(err => {
        console.log("err " + err);
    
      })
    }

  }).catch(err=>{})

  // res.render('login', { rooms: rooms })
})

app.get('/msg', (req, res) => {
  // res.json({q:req.query.room_id})
  console.log("room_id: " + req.query.room_id);
  console.log("user_id: " + req.query.user_id);

  
  // console.log("user_email: " + req.params.user_email);
pool.query("select * from messages where room_id='"+req.query.room_id+"'").then(rows=>{
  
  res.render('chat', {
    roomName: req.query.room_id,
    userID:req.query.user_id,
    data:rows
  })

}).catch(err=>{})

})


server.listen(3000)
// var users ={}
io.on('connection', socket => {
  socket.on('new-user', (room, user_id) => {
  console.log("in connection room="+room);
  console.log("in connection name="+user_id);

    socket.join(room)

    rooms[room].users[socket.id] = user_id
    socket.to(room).broadcast.emit('user-connected', user_id)
  })
  socket.on('send-chat-message', (room,userID, message) => {
    console.log("server side chat mesage ");

    pool.query("select * from rooms where room_id='"+room+"'").then(rows=>{
      console.log("reciever selected ");
      
      var reciever_id;
      var id=0;
      if(rows[0].user_1 != userID){
        reciever_id=rows[0].user_1;
      }
      else{
        reciever_id=rows[0].user_2;
      }
    pool.query("INSERT INTO messages value(?,?,?,?,?)",[reciever_id,message,id,userID,room]).then(rows=>{
      console.log("record inserted ");

      socket.to(room).broadcast.emit('chat-message', {
        message: message,
        name: userID
        // name: rooms[room].users[socket.id]
      })
    }).catch(err=>{})

    }).catch(err=>{})

  })
  socket.on('disconnect', () => {
    getUserRooms(socket).forEach(room => {
      socket.to(room).broadcast.emit('user-disconnected', rooms[room].users[socket.id])
      delete rooms[room].users[socket.id]
    })
  })
})

function getUserRooms(socket) {
  return Object.entries(rooms).reduce((names, [name, room]) => {
    if (room.users[socket.id] != null) names.push(name)
    return names
  }, [])
}


module.exports = router;