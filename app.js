#!/usr/bin/env node

/**
 * Module dependencies.
 */

// var app = require('../app');
var debug = require('debug')('match:server');
var http = require('http');
var socketio = require('socket.io');
var express = require('express');

var app = express();
/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

var server = http.createServer(app)
var io = socketio(server);
/**
 * Create HTTP server.
 */

// var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string' ?
    'Pipe ' + port :
    'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string' ?
    'pipe ' + addr :
    'port ' + addr.port;
  debug('Listening on ' + bind);
}
























var createError = require('http-errors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var cors = require('cors')
var fs = require('fs');

// var http = require('http');

// var debug = require('debug')('match:server');




var pool = require('./db')
// const server = require('http').Server(app)
// const io = require('socket.io')(server)


// server.listen(3000 || process.env.PORT)


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var signupRouter = require('./routes/signup');
var userRouter = require('./routes/user');
var logoutRouter = require('./routes/logout');
// var socketRouter = require('./routes/socket_chat');




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());


// app.use(session({secret:"anyvalue",resave:false,saveUninitialized:true}));


// app.use('/', socketRouter);
app.use('/index', indexRouter);
app.use('/users', usersRouter);
app.use('/signup', signupRouter);
app.use('/user', userRouter);
app.use('/logout', logoutRouter);
// app.use('/chat', socketRouter);

const rooms = {}


app.get('/', (req, res) => {
  res.render('loginn', {
    rooms: rooms
  })
})

// When user click on chat button
app.get('/chats', (req, res) => {
  var user_id = req.query.user_1;
  var arr = [];
  var arr2 = [];

  pool.query("SELECT user_1,user_2,room_id FROM rooms WHERE user_1='" + user_id + "' OR user_2='" + user_id + "' ").then(async rows => {
    var leng = rows.length;
    console.log("rows =" + leng);
    var lenn = leng - 1
    if (rows.length >= 1) {

      rooms[rows[lenn].room_id] = {
        users: {}
      }

      console.log("length of side bar= " +leng )
      var i;
      for (i = 0; i < leng; i++) {
        console.log("in for " + rows[i].user_1);


        if (rows[i].user_1 == req.query.user_1) {
          console.log("in for in if " + rows[i].user_2);

          const rows2 = await pool.query("select name,id,profile_photo_path from users where id='" + rows[i].user_2 + "'");
          const result1 = await pool.query("select message from messages where (sender_id='" + rows[i].user_2 + "' OR reciever_id='" + rows[i].user_2 + "') AND (sender_id='" + rows[i].user_1 + "' OR reciever_id='" + rows[i].user_1 + "'  )");
          var len = result1.length;
          if (len != 0) {
            len = len - 1;
            console.log("Last Message " + result1[len].message);
            rows2[0].last_message = result1[len].message;
            if (rows2[0].profile_photo_path == null || rows2[0].profile_photo_path == "") {
              console.log("image not exist");
            } else {
              const path = 'public/images/' + rows2[0].profile_photo_path;
              const bitmap = fs.readFileSync(path);
              var image = bitmap.toString('base64');
              rows2[0].profile_photo = "data:image/png;base64," + image;
            }
            arr.push(rows2[0]);
          }
        } else {
          console.log("hi");
          console.log("in for in else " + rows[i].user_1);

          const rows3 = await pool.query("select name,id,profile_photo_path from users where id='" + rows[i].user_1 + "'");
          const result2 = await pool.query("select message from messages where (sender_id='" + rows[i].user_2 + "' OR reciever_id='" + rows[i].user_2 + "') AND (sender_id='" + rows[i].user_1 + "' OR reciever_id='" + rows[i].user_1 + "'  )");
          var len = result2.length;
          console.log("length " + len);
          if (len != 0) {
            console.log("in if for last massages");
            len = len - 1;
            console.log("Last Message in else " + result2[len].message);
            rows3[0].last_message = result2[len].message;
            console.log("LAST MESSAGE = " + result2[0].last_message);
            if (rows3[0].profile_photo_path == null || rows3[0].profile_photo_path == "") {
              console.log("image not exist");
            } else {
              const path = 'public/images/' + rows3[0].profile_photo_path;
              const bitmap = fs.readFileSync(path);
              var image = bitmap.toString('base64');
              rows3[0].profile_photo = "data:image/png;base64," + image;
            }
            arr.push(rows3[0]);

          }




        }
      }
      pool.query("select room_id,sender_id,reciever_id from messages where sender_id='" + req.query.user_1 + "' or reciever_id='" + req.query.user_1 + "'").then(rows4 => {
        console.log("In mesage query");
        if (rows4.length >= 1) {
          var len = rows4.length;
          len = len - 1;
          var room_id = rows4[len].room_id;
          console.log("selected room_id = " + room_id);
          console.log("Room ID = " + rows4[len].room_id);

          rooms[rows4[len].room_id] = {
            users: {}
          }

          if (rows4[len].sender_id == req.query.user_1) {
            pool.query("select name,year,location,country,profile_photo_path from users where id='" + rows4[len].reciever_id + "'").then(rows5 => {
              const dt = new Date();
              var current_year = dt.getFullYear();
              rows5[0].age = current_year - rows5[0].year;
              if (rows5[0].profile_photo_path == null || rows5[0].profile_photo_path == "") {
                console.log("image not exist");
              } else {
                const path = 'public/images/' + rows5[0].profile_photo_path;
                const bitmap = fs.readFileSync(path);
                var image = bitmap.toString('base64');
                rows5[0].profile_photo = "data:image/png;base64," + image;
              }

              pool.query("SELECT * FROM messages WHERE room_id='" + room_id + "'").then(rows6 => {

                res.contentType('text/plain');

                res.send({
                  roomName: room_id,
                  userID: req.query.user_1,
                  data: rows6,
                  reciever_data: rows5,
                  sidebar_data: arr
                });

              }).catch(err => {
                console.log("Error while retrieving messages");

              })


            }).catch(err => {
              console.log("error while retrieing reciever data in if statement " + err);

            })



          } else {
            pool.query("select name,year,location,country,profile_photo_path from users where id='" + rows4[len].sender_id + "'").then(rows5 => {
              const dt = new Date();
              var current_year = dt.getFullYear();
              rows5[0].age = current_year - rows5[0].year;
              if (rows5[0].profile_photo_path == null || rows5[0].profile_photo_path == "") {
                console.log("image not exist");
              } else {
                const path = 'public/images/' + rows5[0].profile_photo_path;
                const bitmap = fs.readFileSync(path);
                var image = bitmap.toString('base64');
                rows5[0].profile_photo = "data:image/png;base64," + image;
              }

              pool.query("SELECT * FROM messages WHERE room_id='" + room_id + "'").then(rows6 => {

                res.contentType('text/plain');

                res.send({
                  roomName: room_id,
                  userID: req.query.user_1,
                  data: rows6,
                  reciever_data: rows5,
                  sidebar_data: arr
                });

              }).catch(err => {
                console.log("Error while retrieving messages");

              })


            }).catch(err => {
              console.log("error while retrieing reciever data in else statement " + err);

            })
          }


        } else {
          res.contentType('text/plain');
          res.send("NO Chats Exist of yhis user")
        }


      }).catch(err => {
        console.log("Error while retrieving room_id from messages");

      })
    } else {
      res.contentType('text/plain');
      res.send("NO Chats Exist")
    }



    // res.render('chat', {
    //   roomName: req.query.room_id,
    //   userID:req.query.user_id,
    //   data:rows
    // })

    // conn.end();
    // console.log("Connection Closed");


  }).catch(err => {

  })

  // conn.end();


})
// app.get('/chats', (req, res) => {
//   var user_id=req.query.user_1;
//   var arr = [];
//   var arr2=[];
  
//   pool.query("SELECT user_1,user_2,room_id FROM rooms WHERE user_1='" + user_id + "' OR user_2='" + user_id + "' ").then(async rows => {
//   var len = rows.length;
//   console.log("rows ="+len);
//   var lenn = len-1
//   if (rows.length >= 1) {
  
//   rooms[rows[lenn].room_id] = {
//   users: {}
//   }
  
//   console.log("length of side bar= " + (len-1))
//   var i;
//   for (i = 0; i < len; i++) {
//   console.log("in for " + rows[i].user_1);
  
  
//   if (rows[i].user_1 == req.query.user_1) {
//   console.log("in for in if " + rows[i].user_2);
  
//   const rows2 = await pool.query("select name,id,profile_photo_path from users where id='" + rows[i].user_2 + "'");
//   if (rows2[0].profile_photo_path == null || rows2[0].profile_photo_path == "") {
//   console.log("image not exist");
//   } else {
//   const path = 'public/images/' + rows2[0].profile_photo_path;
//   const bitmap = fs.readFileSync(path);
//   var image = bitmap.toString('base64');
//   rows2[0].profile_photo = "data:image/png;base64," + image;
//   }
//   arr.push(rows2[0]);
//   } else {
//   console.log("hi");
//   console.log("in for in else " + rows[i].user_1);
  
//   const rows3 = await pool.query("select name,id,profile_photo_path from users where id='" + rows[i].user_1 + "'");
//   if (rows3[0].profile_photo_path == null || rows3[0].profile_photo_path == "") {
//   console.log("image not exist");
//   } else {
//   const path = 'public/images/' + rows3[0].profile_photo_path;
//   const bitmap = fs.readFileSync(path);
//   var image = bitmap.toString('base64');
//   rows3[0].profile_photo = "data:image/png;base64," + image;
//   }
//   arr.push(rows3[0]);
  
//   }
//   }
//   pool.query("select room_id,sender_id,reciever_id from messages where sender_id='"+req.query.user_1+"' or reciever_id='"+req.query.user_1+"'").then(rows4=>{
//   console.log("In mesage query");
//   if(rows4.length>=1){
//   var len = rows4.length;
//   len=len-1;
//   var room_id = rows4[len].room_id;
//   console.log("selected room_id = "+room_id);
//   console.log("Room ID = "+rows4[len].room_id);
  
//   rooms[rows4[len].room_id] = {
//   users: {}
//   }
  
//   if(rows4[len].sender_id == req.query.user_1){
//   pool.query("select name,year,location,country,profile_photo_path from users where id='" + rows4[len].reciever_id + "'").then(rows5=>{
//   const dt = new Date();
//   var current_year = dt.getFullYear();
//   rows5[0].age = current_year - rows5[0].year;
//   if (rows5[0].profile_photo_path == null || rows5[0].profile_photo_path == "") {
//   console.log("image not exist");
//   } else {
//   const path = 'public/images/' + rows5[0].profile_photo_path;
//   const bitmap = fs.readFileSync(path);
//   var image = bitmap.toString('base64');
//   rows5[0].profile_photo = "data:image/png;base64," + image;
//   }
  
//   pool.query("SELECT * FROM messages WHERE room_id='"+room_id+"'").then(rows6=>{
  
//   res.contentType('text/plain');
  
//   res.send({
//   roomName: room_id,
//   userID: req.query.user_1,
//   data: rows6,
//   reciever_data: rows5,
//   sidebar_data: arr
//   });
  
//   }).catch(err=>{
//   console.log("Error while retrieving messages");
  
//   })
  
  
//   }).catch(err=>{
//   console.log("error while retrieing reciever data in if statement "+err);
  
//   })
  
  
  
//   }
//   else{
//   pool.query("select name,year,location,country,profile_photo_path from users where id='" + rows4[len].sender_id + "'").then(rows5=>{
//   const dt = new Date();
//   var current_year = dt.getFullYear();
//   rows5[0].age = current_year - rows5[0].year;
//   if (rows5[0].profile_photo_path == null || rows5[0].profile_photo_path == "") {
//   console.log("image not exist");
//   } else {
//   const path = 'public/images/' + rows5[0].profile_photo_path;
//   const bitmap = fs.readFileSync(path);
//   var image = bitmap.toString('base64');
//   rows5[0].profile_photo = "data:image/png;base64," + image;
//   }
  
//   pool.query("SELECT * FROM messages WHERE room_id='"+room_id+"'").then(rows6=>{
  
//   res.contentType('text/plain');
  
//   res.send({
//   roomName: room_id,
//   userID: req.query.user_1,
//   data: rows6,
//   reciever_data: rows5,
//   sidebar_data: arr
//   });
  
//   }).catch(err=>{
//   console.log("Error while retrieving messages");
  
//   })
  
  
//   }).catch(err=>{
//   console.log("error while retrieing reciever data in else statement "+err);
  
//   })
//   }
  
  
//   }
//   else{
//   res.contentType('text/plain');
//   res.send("NO Chats Exist of yhis user")
//   }
  
  
//   }).catch(err=>{
//   console.log("Error while retrieving room_id from messages");
  
//   })
//   }
//   else{
//   res.contentType('text/plain');
//   res.send("NO Chats Exist")
//   }
  
  
  
//   // res.render('chat', {
//   // roomName: req.query.room_id,
//   // userID:req.query.user_id,
//   // data:rows
//   // })
  
//   // conn.end();
//   // console.log("Connection Closed");
  
  
//   }).catch(err => {
  
//   })
  
//   // conn.end();
  
  
//   })


app.post('/loginn', (req, res) => {
  var id = req.body.id
  console.log("user_id= " + id);

      pool.query("select * from users where id  NOT IN ('" + id + "')").then(rows => {
        res.render('allusers', {
          users: rows,
          user_id: id
        })
      }).catch(err => {
        console.log("Error " + err);
      });
      // conn.end();
 
})


// app.get('/create_room', (req, res) => {
//   var user_1 = req.query.user_1;
//   var user_2 = req.query.user_2;
//   var room_id = 0;
//   var arr = [];
//   var arr2=[];
// if(req.query.user_2 =="" || req.query.user_2 ==null || req.query.user_2 ==undefined){
//   console.log("in if statement(where only user id exist) user_id= "+req.query.user_1);
  
//       pool.query("select user_1,user_2, room_id from rooms where user_1='" + user_1 + "' or user_2='" + user_1 + "' ").then(async rows => {
//         var len = rows.length;
//         if (rows.length >= 1) {
//           rooms[rows[0].room_id] = {
//             users: {}
//           }
//           var q = rows[0].room_id;

//           console.log("Room_id=" + q);
//         }
//         console.log("length of side bar= " + len)
//         var i;
//         for (i = 0; i < len; i++) {
//           console.log("in for " + rows[i].user_1);

          
//           if (rows[i].user_1 == req.query.user_1) {
//             console.log("in for in if " + rows[i].user_2);

//             const rows2 = await pool.query("select name,id,profile_photo_path from users where id='" + rows[i].user_2 + "'");
//             if (rows2[0].profile_photo_path == null || rows2[0].profile_photo_path == "") {
//               console.log("image not exist");
//             } else {
//               const path = 'public/images/' + rows2[0].profile_photo_path;
//               const bitmap = fs.readFileSync(path);
//               var image = bitmap.toString('base64');
//               rows2[0].profile_photo = "data:image/png;base64," + image;
//             }
//             arr.push(rows2[0]);
//           } else {
//             console.log("hi");
//             console.log("in for in else " + rows[i].user_1);

//             const rows3 = await pool.query("select name,id,profile_photo_path from users where id='" + rows[i].user_1 + "'");
//             if (rows3[0].profile_photo_path == null || rows3[0].profile_photo_path == "") {
//               console.log("image not exist");
//             } else {
//               const path = 'public/images/' + rows3[0].profile_photo_path;
//               const bitmap = fs.readFileSync(path);
//               var image = bitmap.toString('base64');
//               rows3[0].profile_photo = "data:image/png;base64," + image;
//             }
//             arr.push(rows3[0]);

//           }
//         }
//         pool.query("SELECT room_id,sender_id,reciever_id FROM messages WHERE sender_id='"+req.query.user_1+"' OR reciever_id='"+req.query.user_1+"'").then(rows4=>{
//           var len = rows4.length;
//           len=len-1;
//           var room_id = rows4[len].room_id;
//           console.log("selected room_id = "+room_id);
//           if(rows4[len].sender_id == req.query.user_1){
//             pool.query("select name,year,location,country,profile_photo_path from users where id='" + rows4[len].reciever_id + "'").then(rows5=>{
//               const dt = new Date();
//               var current_year = dt.getFullYear();
//               rows5[0].age = current_year - rows5[0].year;
//               if (rows5[0].profile_photo_path == null || rows5[0].profile_photo_path == "") {
//                 console.log("image not exist");
//               } else {
//                 const path = 'public/images/' + rows5[0].profile_photo_path;
//                 const bitmap = fs.readFileSync(path);
//                 var image = bitmap.toString('base64');
//                 rows5[0].profile_photo = "data:image/png;base64," + image;
//               }

//               pool.query("SELECT * FROM messages WHERE room_id='"+room_id+"'").then(rows6=>{
            
//                 res.contentType('text/plain');
    
//             res.send({
//               roomName: room_id,
//               userID: req.query.user_1,
//               data: rows6,
//               reciever_data: rows5,
//               sidebar_data: arr
//             });
                
//               }).catch(err=>{
//                 console.log("Error while retrieving messages");
                
//               })


//             }).catch(err=>{
//               console.log("error while retrieing reciever data in if statement "+err);
              
//             })



//           }
//           else{
//             pool.query("select name,year,location,country,profile_photo_path from users where id='" + rows4[len].sender_id + "'").then(rows5=>{
//               const dt = new Date();
//           var current_year = dt.getFullYear();
//           rows5[0].age = current_year - rows5[0].year;
//           if (rows5[0].profile_photo_path == null || rows5[0].profile_photo_path == "") {
//             console.log("image not exist");
//           } else {
//             const path = 'public/images/' + rows5[0].profile_photo_path;
//             const bitmap = fs.readFileSync(path);
//             var image = bitmap.toString('base64');
//             rows5[0].profile_photo = "data:image/png;base64," + image;
//           }

//           pool.query("SELECT * FROM messages WHERE room_id='"+room_id+"'").then(rows6=>{
            
//             res.contentType('text/plain');

//         res.send({
//           roomName: room_id,
//           userID: req.query.user_1,
//           data: rows6,
//           reciever_data: rows5,
//           sidebar_data: arr
//         });
            
//           }).catch(err=>{
//             console.log("Error while retrieving messages");
            
//           })


//             }).catch(err=>{
//               console.log("error while retrieing reciever data in else statement "+err);
              
//             })
//           }

          
         

          
//         }).catch(err=>{
//           console.log("Error while retrieving room_id from messages");
          
//         })



        
//         // res.render('chat', {
//         //   roomName: req.query.room_id,
//         //   userID:req.query.user_id,
//         //   data:rows
//         // })

//         // conn.end();
//         // console.log("Connection Closed");
  

//       }).catch(err => {

//       })
    

// }
// else if(req.query.user_2 !="" && req.query.user_2 !=null && req.query.user_2 !=undefined)
// {
//   console.log("in else if statement(where both id exist) user_id= "+req.query.user_1);

//       pool.query("select * from rooms where user_1='" + user_1 + "' AND user_2='" + user_2 + "' OR user_1='" + user_2 + "' AND user_2='" + user_1 + "'").then(rows => {
//         if (rows.length >= 1) {
//           rooms[rows[0].room_id] = {
//             users: {}
//           }
//           var q = rows[0].room_id;

//           console.log("Q=" + q);

//           res.redirect('/msg?room_id=' + rows[0].room_id + '&user_id=' + user_1 + '&user_id2=' + user_2)
//           // io.emit('room-created', rows[0].id)
//         } else {
//           pool.query("INSERT INTO rooms value(?,?,?)", [room_id, user_1, user_2]).then(rows => {
//             console.log("new room created in data base");
//             pool.query("select room_id from rooms where user_1= '" + user_1 + "' AND user_2= '" + user_2 + "'").then(rows => {
//               console.log("id " + rows[0].room_id);
//               rooms[rows[0].room_id] = {
//                 users: {}
//               }
//               res.redirect('/msg?room_id=' + rows[0].room_id + '&user_id=' + user_1 + '&user_id2=' + user_2)
//               // io.emit('room-created', rows[0].id)

//             }).catch(err => {
//               console.log("err " + err);

//             })

//           }).catch(err => {
//             console.log("err " + err);

//           })
//         }

//       }).catch(err => {})
 
//     }
//   // res.render('login', { rooms: rooms })
// })

app.get('/create_room', (req, res) => {
  var user_1 = req.query.user_1;
  var user_2 = req.query.user_2;
  var room_id = 0;


      pool.query("select * from rooms where user_1='" + user_1 + "' AND user_2='" + user_2 + "' OR user_1='" + user_2 + "' AND user_2='" + user_1 + "'").then(rows => {
        if (rows.length >= 1) {
          rooms[rows[0].room_id] = {
            users: {}
          }
          var q = rows[0].room_id;

          console.log("Q=" + q);

          res.redirect('/msg?room_id=' + rows[0].room_id + '&user_id=' + user_1 + '&user_id2=' + user_2)
          // io.emit('room-created', rows[0].id)
        } else {
          pool.query("INSERT INTO rooms value(?,?,?)", [room_id, user_1, user_2]).then(rows => {
            console.log("new room created in data base");
            pool.query("select room_id from rooms where user_1= '" + user_1 + "' AND user_2= '" + user_2 + "'").then(rows => {
              console.log("id " + rows[0].room_id);
              rooms[rows[0].room_id] = {
                users: {}
              }
              res.redirect('/msg?room_id=' + rows[0].room_id + '&user_id=' + user_1 + '&user_id2=' + user_2)
              // io.emit('room-created', rows[0].id)

            }).catch(err => {
              console.log("err " + err);

            })

          }).catch(err => {
            console.log("err " + err);

          })
        }

      }).catch(err => {})
 

  // res.render('login', { rooms: rooms })
})
app.get('/msg', (req, res) => {
  var arr = [];
  // res.json({q:req.query.room_id})
  console.log("room_id: " + req.query.room_id);
  console.log("user_id: " + req.query.user_id);
  console.log("user_id2(reciever): " + req.query.user_id2);
  // console.log("user_email: " + req.params.user_email);

  pool.query("select * from messages where room_id='" + req.query.room_id + "'").then(rows => {
    pool.query("select name,year,location,country,profile_photo_path from users where id='" + req.query.user_id2 + "'").then(rows2 => {
      const dt = new Date();
      var current_year = dt.getFullYear();
      rows2[0].age = current_year - rows2[0].year;
      if (rows2[0].profile_photo_path == null || rows2[0].profile_photo_path == "") {
        console.log("image not exist");
      } else {
        const path = 'public/images/' + rows2[0].profile_photo_path;
        const bitmap = fs.readFileSync(path);
        var image = bitmap.toString('base64');
        rows2[0].profile_photo = "data:image/png;base64," + image;
      }
      pool.query("select user_1,user_2 from rooms where user_1='" + req.query.user_id + "' or user_2='" + req.query.user_id + "' ").then(async rows3 => {
        var leng = rows3.length;
        console.log("length of side bar= " + leng)
        var i;
        for (i = 0; i < leng; i++) {
          console.log("in for " + rows3[i].user_1);

          if (rows3[i].user_1 == req.query.user_id) {
            console.log("in for in if " + rows3[i].user_2);
            const rows4 = await pool.query("select name,id,profile_photo_path from users where id='" + rows3[i].user_2 + "'");
            const rows6 = await pool.query("select message from messages where (sender_id='" + rows3[i].user_2 + "' OR reciever_id='" + rows3[i].user_2 + "') AND (sender_id='" + rows3[i].user_1 + "' OR reciever_id='" + rows3[i].user_1 + "'  )");
            var len = rows6.length;
            if (len != 0) {
              len = len - 1;
              console.log("Last Message " + rows6[len].message);
              rows4[0].last_message = rows6[len].message;
              if (rows4[0].profile_photo_path == null || rows4[0].profile_photo_path == "") {
                console.log("image not exist");
              } else {
                const path = 'public/images/' + rows4[0].profile_photo_path;
                const bitmap = fs.readFileSync(path);
                var image = bitmap.toString('base64');
                rows4[0].profile_photo = "data:image/png;base64," + image;
              }
              arr.push(rows4[0]);

            }

          } else {
            console.log("hi");
            console.log("in for in else " + rows3[i].user_1);

            const rows5 = await pool.query("select name,id,profile_photo_path from users where id='" + rows3[i].user_1 + "'");
            const rows7 = await pool.query("select message from messages where (sender_id='" + rows3[i].user_2 + "' OR reciever_id='" + rows3[i].user_2 + "') AND (sender_id='" + rows3[i].user_1 + "' OR reciever_id='" + rows3[i].user_1 + "'  )");
            var len = rows7.length;
            console.log("length " + len);
            if (len != 0) {
              console.log("in if for last massages");

              len = len - 1;
              console.log("Last Message in else " + rows7[len].message);
              rows5[0].last_message = rows7[len].message;
              console.log("LAST MESSAGE = " + rows5[0].last_message);
              if (rows5[0].profile_photo_path == null || rows5[0].profile_photo_path == "") {
                console.log("image not exist");
              } else {
                const path = 'public/images/' + rows5[0].profile_photo_path;
                const bitmap = fs.readFileSync(path);
                var image = bitmap.toString('base64');
                rows5[0].profile_photo = "data:image/png;base64," + image;
              }
              arr.push(rows5[0]);

            }


          }

        }
        console.log("ARRAY=" + arr);

        res.contentType('text/plain');

        res.send({
          roomName: req.query.room_id,
          userID: req.query.user_id,
          data: rows,
          reciever_data: rows2,
          sidebar_data: arr
        });
        // res.render('chat', {
        //   roomName: req.query.room_id,
        //   userID:req.query.user_id,
        //   data:rows
        // })
      }).catch(err => {})
    }).catch(err => {
      console.log("ERR= " + err);

    })

  }).catch(err => {})

})
// app.get('/msg', (req, res) => {
//   var arr = [];
//   // res.json({q:req.query.room_id})
//   console.log("room_id: " + req.query.room_id);
//   console.log("user_id: " + req.query.user_id);
//   console.log("user_id2(reciever): " + req.query.user_id2);
//   // console.log("user_email: " + req.params.user_email);

//       pool.query("select * from messages where room_id='" + req.query.room_id + "'").then(rows => {
//         pool.query("select name,year,location,country,profile_photo_path from users where id='" + req.query.user_id2 + "'").then(rows2 => {
//           const dt = new Date();
//           var current_year = dt.getFullYear();
//           rows2[0].age = current_year - rows2[0].year;
//           if (rows2[0].profile_photo_path == null || rows2[0].profile_photo_path == "") {
//             console.log("image not exist");
//           } else {
//             const path = 'public/images/' + rows2[0].profile_photo_path;
//             const bitmap = fs.readFileSync(path);
//             var image = bitmap.toString('base64');
//             rows2[0].profile_photo = "data:image/png;base64," + image;
//           }
//           pool.query("select user_1,user_2 from rooms where user_1='" + req.query.user_id + "' or user_2='" + req.query.user_id + "' ").then(async rows3 => {
//             var len = rows3.length;
//             console.log("length of side bar= " + len)
//             var i;
//             for (i = 0; i < len; i++) {
//               console.log("in for " + rows3[i].user_1);

//               if (rows3[i].user_1 == req.query.user_id) {
//                 console.log("in for in if " + rows3[i].user_2);

//                 const rows4 = await pool.query("select name,id,profile_photo_path from users where id='" + rows3[i].user_2 + "'");
//                 if (rows4[0].profile_photo_path == null || rows4[0].profile_photo_path == "") {
//                   console.log("image not exist");
//                 } else {
//                   const path = 'public/images/' + rows4[0].profile_photo_path;
//                   const bitmap = fs.readFileSync(path);
//                   var image = bitmap.toString('base64');
//                   rows4[0].profile_photo = "data:image/png;base64," + image;
//                 }
//                 arr.push(rows4[0]);
//               } else {
//                 console.log("hi");
//                 console.log("in for in else " + rows3[i].user_1);

//                 const rows5 = await pool.query("select name,id,profile_photo_path from users where id='" + rows3[i].user_1 + "'");
//                 if (rows5[0].profile_photo_path == null || rows5[0].profile_photo_path == "") {
//                   console.log("image not exist");
//                 } else {
//                   const path = 'public/images/' + rows5[0].profile_photo_path;
//                   const bitmap = fs.readFileSync(path);
//                   var image = bitmap.toString('base64');
//                   rows5[0].profile_photo = "data:image/png;base64," + image;
//                 }
//                 arr.push(rows5[0]);

//               }
//             }
//             res.contentType('text/plain');

//             res.send({
//               roomName: req.query.room_id,
//               userID: req.query.user_id,
//               data: rows,
//               reciever_data: rows2,
//               sidebar_data: arr
//             });
//             // res.render('chat', {
//             //   roomName: req.query.room_id,
//             //   userID:req.query.user_id,
//             //   data:rows
//             // })

         
      

//           }).catch(err => {})
//         }).catch(err => {
//           console.log("ERR= " + err);

//         })

//       }).catch(err => {})

// })


io.on('connection', socket => {
  socket.on('new-user', (room, user_id) => {
    console.log("in connection room=" + room);
    console.log("in connection name=" + user_id);

    socket.join(room)

    rooms[room].users[socket.id] = user_id
    socket.to(room).broadcast.emit('user-connected', user_id)
  })
  socket.on('send-chat-message', (room, userID, message) => {
    console.log("server side chat mesage ");


        pool.query("select * from rooms where room_id='" + room + "'").then(rows => {
          console.log("reciever selected ");
          console.log("sender Id " + userID);


          var reciever_id;
          var id = 0;
          if (rows[0].user_1 != userID) {
            reciever_id = rows[0].user_1;
          } else {
            reciever_id = rows[0].user_2;
          }
          console.log("Reciever Id " + reciever_id);
          console.log("Message : " + message);
          console.log("UserID : " + userID);
          console.log("room : " + room);

          pool.query("INSERT INTO messages value(?,?,?,?,?)", [reciever_id, message, id, userID, room]).then(rows => {
            console.log("record inserted ");

            // io.in('game').emit('big-announcement', 'the game will start soon');
            // socket.to(room).broadcast.emit('chat-message', {

            io.in(room).emit('chat-message', {
              message: message,
              name: userID
              // name: rooms[room].users[socket.id]
            })
          }).catch(err => {})

        }).catch(err => {})


  })
  socket.on('disconnect', () => {
    getUserRooms(socket).forEach(room => {
      socket.to(room).broadcast.emit('user-disconnected', rooms[room].users[socket.id])
      delete rooms[room].users[socket.id]
    })
  })
})

function getUserRooms(socket) {
  return Object.entries(rooms).reduce((names, [name, room]) => {
    if (room.users[socket.id] != null) names.push(name)
    return names
  }, [])
}

app.get('/*', function (req, res) {
  res.status(404).send('Page Not Exist');
});


app.post('/*', function (req, res) {
  res.status(404).send('Page Not Exist');
});




// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});















module.exports = app;